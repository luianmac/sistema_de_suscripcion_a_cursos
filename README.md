# Sistema de suscripcion a cursos
Este proyecto es un sistema CRUD que permite a los usuarios suscribirse a cursos.

existen 3 tipos de usuarios:
- Administrador
> Solo existe un usuario administrador que crear a los usuarios creadores
> usuario: admin@admin.com
> contraseña: 1234567

- Creador
> Pueden crear cursos y asignarles uno de estos estados:
> - Activo
> - Inactivo
> - En construcción

> Solo pueden tener máximo dos cursos activos en línea

- Consumidor
> Pueden ingresar creandose su propia cuenta.

## Características
Este CRUD fue elaborado con:
- XAMPP 8.1.6
- phpMyAdmin SQL 5.2.0
- PHP 8.1.6
- MariaDB 10.4.24

## Instalación
** Primera Forma**
1. Instalar XAMPP para Windows
2. Crear la base de datos *"suscripcion"* en phpmyadmin y luego importe las tablas mediante el archivo suscripcion.sql 
3. Clonar este proyecto en la ruta \xampp\htdocs de su computadora mediante el comando:
 `git clone https://gitlab.com/luianmac/sistema_de_suscripcion_a_cursos.git `
5. Abrir XAMPP e inicie los servicios de Apache y MySQL
6. Ingrese al sistema escribiendo http://localhost/sistema_de_suscripcion_a_cursos/ en su navegador.

**Segunda forma:**
1. Descargar la carpeta Docker
2. Abrir una terminal en la ruta de la carpeta Docker y ejecutar el siguiente comando:
`docker-compose up -d`
4. Accede al CRUD colocando en el navegador: http://localhost:80/



