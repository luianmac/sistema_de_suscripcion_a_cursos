<?php

function isEmptyLogin($usuario,$contrasena){
    if(strlen(trim($usuario)) <1 || strlen(trim($contrasena)) <1){
        return true;
    }else{
        return false;
    }
}

function login($usuario,$contrasena){
    global $mysqli;
    $stmt = $mysqli->prepare("SELECT id,nombre,apellido,contrasena,id_tipo FROM usuarios WHERE usuario = ?");
    $stmt->bind_param('s',$usuario);
    $stmt->execute();
    $stmt->store_result();
    $rows = $stmt->num_rows;

    if($rows > 0){
        $stmt->bind_result($id,$nombre,$apellido,$contrasenadb,$id_tipo);
        $stmt->fetch();
        $contrasena_c = sha1($contrasena);

        if( $contrasena_c == $contrasenadb){
            $_SESSION['id'] = $id;
            $_SESSION['nombre'] = $nombre;
            $_SESSION['apellido'] = $apellido;
            $_SESSION['id_tipo'] = $id_tipo;

            /*Redirecciono a una pagina deacuerdo al usuario*/
            if($id_tipo == 1){
                header("Location: admin.php");
            }else if($id_tipo == 2){
                header("Location: creadores.php");
            }elseif($id_tipo == 3){
                header("Location: consumidores.php");
            }
        }else{
            $errors = "Las contraseña no coinciden";
        }
    }else{
        $errors = "Usuario no existe";
    }
    return $errors;
}
function resultBlock($errors){
    if(count($errors)>0){
        echo "<div id='error' class='alert alert-danger' role='alert'>
        <a href='#' onclick=\"showHide('error');\">[x]</a>
        <ul>";
        foreach($errors as $error){
            echo "<li>".$error."</li>";
        }
        echo "</ul>";
        echo "</div>";
    }
}

function isEmptyRegistro($nombre,$apellido,$correo,$contrasena,$confContrasena){
    if(strlen(trim($nombre)) < 1 || strlen(trim($apellido)) < 1 ||
    strlen(trim($correo)) < 1 ||strlen(trim($contrasena)) < 1 ||strlen(trim($confContrasena)) < 1){
        return true;
    }else{
        return false;
    }
}

function isMail($usuario){
    if(filter_var($usuario,FILTER_VALIDATE_EMAIL)){
        return true;
    }else{
        return false;
    }
}

function encriptarContrasena($contrasena){
    $contrasena_e = sha1($contrasena);
    return $contrasena_e;
}

function registrarUsuario($nombre,$apellido,$usuario,$contrasena_e,$tipo_usuario){
    global $mysqli ;
    $stmt = $mysqli->prepare("INSERT INTO usuarios (usuario,nombre,apellido,contrasena,id_tipo) VALUES (?,?,?,?,?)");
    $stmt->bind_param('ssssi',$usuario,$nombre,$apellido,$contrasena_e,$tipo_usuario);
    if($stmt->execute()){
        return $mysqli->insert_id;  //el id que del usuario que se agrego
    }else{
        return 0;
    }
    
}
function isEmptyCurso($name_curso,$precio,$estado){
    if(strlen(trim($name_curso)) < 1 || strlen(trim($precio)) <1 ||
    strlen(trim($estado))<1 ){
        return true;
    }else{
        return false;
    }
}

function creacionCurso($name_curso,$precio,$estado,$id){
    global $mysqli ;
    $stmt = $mysqli->prepare("INSERT INTO cursos (nombre_curso,precio,estado,id_usuario) VALUES (?,?,?,?)");
    $stmt->bind_param('sssi',$name_curso,$precio,$estado,$id);
    if($stmt->execute()){
        return 1;  //el id que del usuario que se agrego
    }else{
        return 0;
    }
}

function NumeroCursosActivos($id){
    global $mysqli;
    $stmt = $mysqli->query("SELECT COUNT(*) FROM cursos WHERE id_usuario=$id AND estado='activo'");
    $row = $stmt->fetch_assoc();
    return $row["COUNT(*)"] ;
}
?>