<?php
    require "funciones.php";
    require "conexion.php";
    $errors = array();

    if(!empty($_POST)){
        $nombre = $mysqli->real_escape_string($_POST['nombre']);
        $apellido = $mysqli->real_escape_string($_POST['apellido']);
        $usuario = $mysqli->real_escape_string($_POST['correo']);
        $contrasena = $mysqli->real_escape_string($_POST['contrasena']);
        $confContrasena = $mysqli->real_escape_string($_POST['confContrasena']);

        $tipo_usuario = 3;

        if(isEmptyRegistro($nombre,$apellido,$usuario,$contrasena,$confContrasena)){
            $errors[] = "Todos los campos deben estar llenos";
        }
        if(!isMail($usuario)){
            $errors[] = "Ingrese un correo válido";
        }
        /**validacion de contrasenas */
        if(strcmp($contrasena,$confContrasena)!==0){
            $errors[] = "Las contraseñas no coinciden";
        }
        if(count($errors)==0){
            $contrasena_e = encriptarContrasena($contrasena);
            $registro = registrarUsuario($nombre,$apellido,$usuario,$contrasena_e,$tipo_usuario);
            if($registro>0){
                echo "USUARIO REGISTRADO CON EXITO";
                //echo "<br><a href='index.php' >Iniciar Sesion</a>";
                header("Location: index.php"); //se redirecciona a login
                exit;
            }else{
                $errors = "Error al registrar";
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Register</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Crear Cuenta</h3></div>
                                    <div class="card-body">
                                        <form method="POST" action="<?php $_SERVER['PHP_SELF']; ?>">
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" name="nombre" id="inputFirstName" type="text" placeholder="Enter your first name" />
                                                        <label for="inputFirstName">Nombre</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating">
                                                        <input class="form-control" name="apellido" id="inputLastName" type="text" placeholder="Enter your last name" />
                                                        <label for="inputLastName">Apellido</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-floating mb-3">
                                                <input class="form-control" name="correo" id="inputEmail" type="email" placeholder="name@example.com" />
                                                <label for="inputEmail">Correo electrónico</label>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" name="contrasena" id="inputPassword" type="password" placeholder="Create a password" />
                                                        <label for="inputPassword">Contraseña</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" name="confContrasena" id="inputPasswordConfirm" type="password" placeholder="Confirm password" />
                                                        <label for="inputPasswordConfirm">Repite Contraseña</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-4 mb-0">
                                                <div class="d-grid"><button type="submit" class="btn btn-primary btn-block" >Crear Cuenta</button></div>
                                            </div>
                                        </form>
                                        <?php echo resultBlock($errors) ?>
                                    </div>
                                    <div class="card-footer text-center py-3">
                                        <div class="small"><a href="index.php">¿Ya tienes una cuenta? ve al login</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2022</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
