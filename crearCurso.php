<?php
    require "conexion.php";
    require "funciones.php";
    session_start();
    $errors = array();
    if(!isset($_SESSION['id'])){
        header("Location: index.php");
    }
    $nombre = $_SESSION['nombre'];
    $apellido = $_SESSION['apellido'];
    $id = $_SESSION['id'];

    if(!empty($_POST)){
        $name_curso = $mysqli->real_escape_string($_POST['name_curso']);
        $precio = $mysqli->real_escape_string($_POST['precio']);
        $estado = $mysqli->real_escape_string($_POST['estado']);

        /*validacion si uno de los campos esta vacio */
        if(isEmptyCurso($name_curso,$precio,$estado)){
            $errors[] = "Todos los campos deben estar llenos";
        }

        if($estado=='activo'){
            $cursosActivos = NumeroCursosActivos($id);
            if($cursosActivos>=2){
                $errors[] = "Solo puede tener 2 cursos activos";
            }
        }
        if(count($errors)==0){
            $registro = creacionCurso($name_curso,$precio,$estado,$id); 
            if($registro>0){
                echo "CURSO REGISTRADO CON EXITO";
                header("Location: creadores.php"); //se redirecciona a login
                exit;
            }else{
                $errors = "Error al registrar";
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Usuario Creador</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>

    </head>
    <body class="bg-primary">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="creadores.php">Creador</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto me-0 me-md-3 my-2 my-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><?php echo "$nombre  $apellido" ?><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#!">Configuración</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="index.php">Salir</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" href="creadores.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>Dashboard</a>
                            <div class="sb-sidenav-menu-heading">Interface</div>
                            <a class="nav-link" href="crearCurso.php" ><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>Crear Curso</a>
                        </div>    
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Start Bootstrap
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Nuevo Curso</h3></div>
                                    <div class="card-body">
                                        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                            <div class="form-floating mb-3">
                                                <input class="form-control" name="name_curso" id="inputCurso" type="text" placeholder="" />
                                                <label for="inputCurso">Nombre del Curso</label>
                                            </div>
                                            <div class="form-floating mb-3">
                                                <input class="form-control" name="precio" id="inputname" type="text" placeholder="" />
                                                <label for="inputname">Precio $</label>
                                            </div>
                                            <div class="form-floating mb-3">
                                                <select class="form-select" aria-label="Estado" name="estado">
                                                    <option selected></option>
                                                    <option value="activo">Activo</option>
                                                    <option value="inactivo">Inactivo</option>
                                                    <option value="construccion">Construccion</option>
                                                </select>
                                            </div>
                                            <div class="mt-4 mb-0">
                                                <div class="d-grid"><button type="submit" class="btn btn-primary" >Crear</button></div>
                                            </div>
                                        </form>
                                        <?php echo resultBlock($errors)?>
                                    </div>
                                    <div class="card-footer text-center py-3">
                                        <div class="small"><a href="creadores.php">Regresar</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2022</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
    </body>
</html>
