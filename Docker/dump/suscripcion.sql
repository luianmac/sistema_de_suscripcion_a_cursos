-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-06-2022 a las 04:33:02
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `suscripcion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `nombre_curso` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `precio` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`nombre_curso`, `precio`, `estado`, `id_usuario`) VALUES
('Azure Fundamentals', '100', 'activo', 6),
('Danza', '50', 'activo', 8),
('Economía Avanzada', '80', 'inactivo', 5),
('Finanzas', '100', 'activo', 5),
('Física 3', '80', 'activo', 4),
('JavaScript', '45', 'activo', 4),
('Matemáticas I', '50', 'construccion', 4),
('Matemáticas II', '60', 'construccion', 4),
('Programacion en C', '45', 'construccion', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL,
  `tipo` varchar(80) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id`, `tipo`) VALUES
(1, 'administrador'),
(2, 'creador'),
(3, 'consumidor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `id_tipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `nombre`, `apellido`, `contrasena`, `id_tipo`) VALUES
(1, 'admin@admin.com', 'admin', '', '20eabe5d64b0e216796e834f52d61fd0b70332fc', 1),
(2, 'luianmac@espol.edu.ec', 'Luis', 'Macas', 'faea5242a00c52da62a0f00df168c199b7ab748d', 3),
(3, 'juliana11@gmail.com', 'Juliana', 'Lopez', '81427a8ca2346669e614430cc07dc2b14fa0adec', 3),
(4, 'mariana@gmail.com', 'Mariana', 'López', '0dcc3cc42445680eb0908b2b10b825b6ac5bb7c8', 2),
(5, 'jc@gmail.com', 'Jean', 'Muñoz', '51f8b1fa9b424745378826727452997ee2a7c3d7', 2),
(6, 'rrruales@gmail.com', 'Richard', 'Ruales', '320bca71fc381a4a025636043ca86e734e31cf8b', 2),
(7, 'cris@gmail.com', 'Cristina', 'Robles', '539ceab9a4c155868dd360023fd9ed09f5ff249b', 3),
(8, 'ines@gmail.com', 'Ines', 'Leiton', 'fd8db66f2f05d9b8731c71baaceec582265a8fa1', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`nombre_curso`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tipo` (`id_tipo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD CONSTRAINT `cursos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_tipo`) REFERENCES `tipo_usuario` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
